//
//  HomeViewController.swift
//  MonwaGuru
//
//  Created by lembah7 on 04/09/20.
//  Copyright © 2020 Moonlight Technology. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController{
    
    @IBOutlet weak var beritaCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        beritaCollectionView.dataSource = self
        beritaCollectionView.delegate = self
        beritaCollectionView.register(UINib(nibName: "BeritaCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BeritaCell")
    }
    
}

extension HomeViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return beritas.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // Digunakan untuh menghubungkan cell dengan identifier "HeroCell"
        if let cell = beritaCollectionView.dequeueReusableCell(withReuseIdentifier: "BeritaCell", for: indexPath) as? BeritaCollectionViewCell {
            
            // Menetapkan nilai hero ke view di dalam cell
            let item = beritas[indexPath.row]
            cell.namaBerita.text = item.name
            cell.judulBerita.text = item.description
            //item.photoHero.image = beritas.photo
            
            // Kode ini digunakan untuk membuat imageView memiliki frame bound/lingkaran
            //cell.photoHero.layer.cornerRadius = cell.photoHero.frame.height / 2
            //cell.photoHero.clipsToBounds = true
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    
}

extension HomeViewController: UICollectionViewDelegate{

    

}
