//
//  Berita.swift
//  MonwaGuru
//
//  Created by lembah7 on 04/09/20.
//  Copyright © 2020 Moonlight Technology. All rights reserved.
//

import UIKit

struct Berita {
    let photo: UIImage
    let name: String
    let description: String
    
}
