//
//  DummyJadwal.swift
//  MonwaGuru
//
//  Created by lembah7 on 04/09/20.
//  Copyright © 2020 Moonlight Technology. All rights reserved.
//

import UIKit

var listJadwal: [Jadwal] = [
    Jadwal(matapelajaran: "Matematika", kelas: "Kelas X A", jam: "07.30 - 08.30"),
    Jadwal(matapelajaran: "Istirahat", kelas: "-", jam: "09.30 - 09.45"),
    Jadwal(matapelajaran: "Fisika", kelas: "Kelas X B", jam: "09.45 - 10.45"),
    Jadwal(matapelajaran: "Matematika", kelas: "Kelas VII C", jam: "10.45 - 11.45")
    
]
