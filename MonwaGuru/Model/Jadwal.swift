//
//  Jadwal.swift
//  MonwaGuru
//
//  Created by lembah7 on 04/09/20.
//  Copyright © 2020 Moonlight Technology. All rights reserved.
//

import UIKit

struct Jadwal {
    let matapelajaran: String
    let kelas: String
    let jam: String
    
}
